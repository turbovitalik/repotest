<?php

class Foo
{
    public function __construct($foo, $bar)
    {
        $this->foo = $foo;
        $this->bar = $bar;
    }

    public function do()
    {
        return $this->foo->do();
    }
}
